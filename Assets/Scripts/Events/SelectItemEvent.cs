﻿using UnityEngine;

public class SelectItemEvent : IEvent
{
    public Item Item;

    public SelectItemEvent( Item item )
    {
        Item = item;
    }
}
