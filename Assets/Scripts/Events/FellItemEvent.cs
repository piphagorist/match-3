﻿using UnityEngine;

public class FellItemEvent : IEvent
{
    public Item Item;

    public FellItemEvent( Item item )
    {
        Item = item;
    }
}
