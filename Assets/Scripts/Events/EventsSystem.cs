﻿using System;
using System.Collections.Generic;



public class EventsSystem
{
    private static Dictionary<Type, Dictionary<int, Action<IEvent>>> _listIFMEvent;
    private static Dictionary<SimpleEvent, List<Action>> _listEventType;


    public static void InitEvents()
    {
        _listIFMEvent = new Dictionary<Type, Dictionary<int, Action<IEvent>>>();
        _listEventType = new Dictionary<SimpleEvent, List<Action>>();
    }

    public static void AddEvent<T>( Action<T> action ) where T : IEvent
    {
        Type t = typeof( T );

        Action<IEvent> a = new Action<IEvent>( o => action( ( T )o ) );

        if ( !_listIFMEvent.ContainsKey( t ) )
            _listIFMEvent.Add( t, new Dictionary<int, Action<IEvent>>() );

        if ( _listIFMEvent[ t ].ContainsKey( action.GetHashCode() ) && _listIFMEvent[ t ][ action.GetHashCode() ] != null )
            return;

        _listIFMEvent[ t ].Add( action.GetHashCode(), a );
    }

    public static void CallEvent<T>( IEvent parameter ) where T : IEvent
    {
        Type t = typeof( T );

        if ( !_listIFMEvent.ContainsKey( t ) ) return;

        foreach ( KeyValuePair<int, Action<IEvent>> pair in _listIFMEvent[ t ] )
            _listIFMEvent[ t ][ pair.Key ]( parameter );
    }

    public static void RemoveEvent<T>( Action<T> action ) where T : IEvent
    {
        Type t = typeof( T );

        _listIFMEvent[ t ].Remove( action.GetHashCode() );
    }

    public static void AddEvent( SimpleEvent type, Action action )
    {
        if ( !_listEventType.ContainsKey( type ) )
            _listEventType.Add( type, new List<Action>() );

        if ( !_listEventType[ type ].Contains( action ) )
            _listEventType[ type ].Add( action );
    }

    public static void CallEvent( SimpleEvent type )
    {
        if ( !_listEventType.ContainsKey( type ) ) return;

        foreach ( Action action in _listEventType[ type ] )
            action();
    }

    public static void RemoveEvent( SimpleEvent type, Action action )
    {
        _listEventType[ type ].Remove( action );
    }
}


public interface IEvent { }


public enum SimpleEvent
{
    InverseGravity,
}