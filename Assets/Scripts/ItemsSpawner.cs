﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ItemsSpawner : MonoBehaviour
{
    public Vector2Int GravityDirection { get { return _gravityDirection; } }

    private Vector2Int _gravityDirection = Vector2Int.up;



    private void Start()
    {
        EventsSystem.AddEvent( SimpleEvent.InverseGravity, OnInverseGravity );
    }

    private void OnInverseGravity()
    {
        _gravityDirection *= -1;
    }


    public Item Create( Item item, Vector2Int index, int firstY )
    {
        Vector2 targetPos = Game.GameField.GetPosByIndex( index );
        Vector2 startPos;

        float delay;

        if ( GravityDirection.y == 1 )
        {
            startPos = new Vector2( targetPos.x, Game.GameField.RTransform.sizeDelta.y + ( Game.Params.CellSize + Game.Params.CellSpacing ) * ( index.y - firstY ) );
        }
        else
        {
            startPos = new Vector2( targetPos.x, -Game.Params.CellSize - ( Game.Params.CellSize + Game.Params.CellSpacing ) * ( firstY - index.y ) );
        }

        Item newItem = Instantiate( item, Game.GameField.RTransform );
        newItem.RTransform.anchoredPosition = startPos;
        if ( item is SpecialItem )
        {
            newItem.RTransform.anchoredPosition = targetPos;
            delay = 0;
        }
        newItem.Init( Game.Params.CellSize, targetPos, index, 0 );

        return newItem;
    }
}
