﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedItem : Item
{
    public RedItem()
    {
        _type = ItemType.Red;
    }
}
