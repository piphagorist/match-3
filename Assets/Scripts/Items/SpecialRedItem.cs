﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialRedItem : SpecialItem
{
    public SpecialRedItem()
    {
        _type = ItemType.Red;
    }
}
