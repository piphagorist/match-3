﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialBlueItem : SpecialItem
{
    public SpecialBlueItem()
    {
        _type = ItemType.Blue;
    }
}
