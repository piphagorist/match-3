﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenItem : Item
{
    public GreenItem()
    {
        _type = ItemType.Green;
    }
}
