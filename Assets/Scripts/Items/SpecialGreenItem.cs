﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialGreenItem : SpecialItem
{
    public SpecialGreenItem()
    {
        _type = ItemType.Green;
    }
}
