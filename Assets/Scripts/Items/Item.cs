﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Item : MonoBehaviour, IPointerClickHandler
{
    public Vector2Int Index { get { return _index; } }

    public Vector2 Position { get { return _targetPosition; } }

    public ItemType Type { get { return _type; } }

    public bool Destroyed { get; private set; }
    public bool Marked { get; private set; }


    protected static bool BLOCKED;


    protected ItemType _type;


    private Vector2 _targetPosition;
    private Vector2 _lastPosition;

    private Vector2Int _index;

    private float _delay;
    private float _creationTime;

    private bool _fell;
    private RectTransform _rTransform;
    public RectTransform RTransform
    {
        get
        {
            if ( !_rTransform )
                _rTransform = ( RectTransform )transform;

            return _rTransform;
        }
    }



    public static void SetBlocking( bool value )
    {
        BLOCKED = value;
    }


    public void Init( float size, Vector2 targetPos, Vector2Int index, float delay )
    {
        RTransform.pivot = RTransform.anchorMin = RTransform.anchorMax = Vector2.zero;
        RTransform.sizeDelta = Vector2.one * size;

        _targetPosition = targetPos;

        _index = index;

        _delay = delay;
        _creationTime = Time.time;
    }

    public void SetIndex( Vector2Int index )
    {
        _index = index;
    }

    public void MoveTowards( Vector2 pos )
    {
        _targetPosition = pos;
    }

    public void OnPointerClick( PointerEventData eventData )
    {
        if ( BLOCKED ) return;

        GetComponent<Image>().color = new Color( 0.75f, 0.75f, 0.75f, 1 );
        EventsSystem.CallEvent<SelectItemEvent>( new SelectItemEvent( this ) );
    }

    public void Unselect()
    {
        GetComponent<Image>().color = new Color( 0, 0, 0, 0 );
    }

    public void Mark()
    {
        Marked = true;
    }

    public virtual void Destroy()
    {
        //EventsSystem.CallEvent<FellItemEvent>( new FellItemEvent( this ) );
        Destroyed = true;
        Destroy( gameObject );
    }


    private void Update()
    {
        if ( Time.time - _creationTime < _delay ) return;

        _lastPosition = Vector2.MoveTowards( RTransform.anchoredPosition, _targetPosition, Time.deltaTime * Game.Params.ItemsSpeed );
        if ( _lastPosition == RTransform.anchoredPosition )
        {
            _fell = true;
            EventsSystem.CallEvent<FellItemEvent>( new FellItemEvent( this ) );
        }
        else
        {
            _fell = false;
            RTransform.anchoredPosition = _lastPosition;
        }
    }
}

public enum ItemType
{
    Blue,
    Green,
    Red,
}
