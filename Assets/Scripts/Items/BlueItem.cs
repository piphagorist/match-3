﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueItem : Item
{
    public BlueItem()
    {
        _type = ItemType.Blue;
    }
}
