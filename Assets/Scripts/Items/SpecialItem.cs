﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialItem : Item
{
    public override void Destroy()
    {
        base.Destroy();
        EventsSystem.CallEvent( SimpleEvent.InverseGravity );
    }
}
