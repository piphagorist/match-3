﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ItemsManager : MonoBehaviour
{
    private ItemsSpawner _itemsSpawner;

    private Item[,] _items;

    private List<Item> _fallingItems = new List<Item>();

    private Item _selectedItem;

    private bool _destroyingProcess;


    private void Awake()
    {
        EventsSystem.AddEvent<SelectItemEvent>( OnSelectItem );
        EventsSystem.AddEvent<FellItemEvent>( OnFellItem );
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds( 1 );

        _itemsSpawner = GetComponent<ItemsSpawner>();

        _items = new Item[ Game.Params.BoardSize.x, Game.Params.BoardSize.y ];

        CreateItems();

        Item.SetBlocking( true );
    }

    private void CreateItems( List<Item> foundItems = null )
    {
        List<Vector2Int> indeces = new List<Vector2Int>();

        List<Item> newItems = new List<Item>();

        for ( int y = 0; y < Game.Params.BoardSize.y; y++ )
            for ( int x = 0; x < Game.Params.BoardSize.x; x++ )
                if ( _items[ x, y ] == null || _items[ x, y ].Destroyed )
                    indeces.Add( new Vector2Int( x, y ) );

        for ( int i = 0; i < indeces.Count; i++ )
        {
            Item lastLeft = indeces[ i ].x == 0 ? null : _items[ indeces[ i ].x - 1, indeces[ i ].y ];
            Item lastBelow = indeces[ i ].y == 0 ? null : _items[ indeces[ i ].x, indeces[ i ].y - 1 ];

            List<ItemType> possibleItemsTypes = new List<ItemType>( Game.ItemOriginByType.Keys );
            if ( lastLeft ) possibleItemsTypes.Remove( lastLeft.Type );
            if ( lastBelow ) possibleItemsTypes.Remove( lastBelow.Type );

            _items[ indeces[ i ].x, indeces[ i ].y ] = Game.ItemOriginByType[ possibleItemsTypes[ Random.Range( 0, possibleItemsTypes.Count ) ] ];
        }

        int startIndex = _itemsSpawner.GravityDirection.y == 1 ? 0 : indeces.Count - 1;
        int finishIndex = _itemsSpawner.GravityDirection.y == 1 ? indeces.Count : -1;

        if ( _itemsSpawner.GravityDirection.y == -1 ) indeces.Reverse();

        for ( int i = 0; i < indeces.Count; i += 1 )
        {
            _items[ indeces[ i ].x, indeces[ i ].y ] = _itemsSpawner.Create( _items[ indeces[ i ].x, indeces[ i ].y ], indeces[ i ], indeces[ 0 ].y );
            newItems.Add( _items[ indeces[ i ].x, indeces[ i ].y ] );
        }

        if ( foundItems != null )
        {
            foreach ( Item item in foundItems )
                if ( !newItems.Contains( item ) ) newItems.Add( item );
        }
        else
            foundItems = new List<Item>();

        if ( newItems.Count > 0 )
        {
            _fallingItems.Clear();
            _fallingItems.AddRange( newItems );

            foreach ( Item item in newItems )
            {
                if ( !foundItems.Contains( item ) )
                    foundItems.AddRange( GetMatchedItems( item ) );
            }
            DestroyItems( foundItems );
        }
    }

    private void MoveItems()
    {
        List<Item> movedItems = new List<Item>();

        if ( _itemsSpawner.GravityDirection.y != 0 )
        {
            int startIndexY = _itemsSpawner.GravityDirection.y == 1 ? 0 : Game.Params.BoardSize.y - 1;
            int finishIndexY = _itemsSpawner.GravityDirection.y == 1 ? Game.Params.BoardSize.y : -1;

            for ( int x = 0; x < Game.Params.BoardSize.x; x++ )
            {
                List<Item> emptyCells = new List<Item>();
                for ( int y = startIndexY; y != finishIndexY; y += _itemsSpawner.GravityDirection.y )
                {
                    if ( _items[ x, y ].Destroyed )
                        emptyCells.Add( _items[ x, y ] );
                    else if ( emptyCells.Count > 0 )
                    {
                        movedItems.Add( _items[ x, y ] );
                        SwapItems( _items[ x, y ], emptyCells[ 0 ] );
                        emptyCells.Add( emptyCells[ 0 ] );
                        emptyCells.RemoveAt( 0 );
                    }
                }
            }
        }

        List<Item> foundItems = new List<Item>();

        foreach ( Item item in movedItems )
        {
            if ( !foundItems.Contains( item ) )
                foundItems.AddRange( GetMatchedItems( item ) );
        }

        CreateItems( foundItems );

    }

    private void OnSelectItem( SelectItemEvent data )
    {
        if ( _destroyingProcess )
        {
            data.Item.Unselect();
            return;
        }

        if ( !_selectedItem )
        {
            _selectedItem = data.Item;
            return;
        }

        if ( data.Item == _selectedItem || data.Item.Type == _selectedItem.Type )
        {
            data.Item.Unselect();
            _selectedItem.Unselect();
            _selectedItem = null;
            return;
        }

        Vector2Int deltaIndex = data.Item.Index - _selectedItem.Index;
        if ( ( deltaIndex.x != 0 && deltaIndex.y != 0 ) || Mathf.Abs( deltaIndex.x ) > 1 || Mathf.Abs( deltaIndex.y ) > 1 )
        {
            data.Item.Unselect();
            return;
        }

        data.Item.Unselect();

        SwapItems( _selectedItem, data.Item );

        List<Item> foundItems = GetMatchedItems( _selectedItem );
        foundItems.AddRange( GetMatchedItems( data.Item ) );

        if ( foundItems.Count == 0 )
        {
            SwapItems( _selectedItem, data.Item );
            return;
        }

        _selectedItem.Unselect();

        Item.SetBlocking( true );

        _fallingItems.Clear();
        _fallingItems.Add( _selectedItem );
        _fallingItems.Add( data.Item );

        DestroyItems( foundItems, 0 );

        _selectedItem = null;
    }

    private List<Item> GetMatchedItems( Item item )
    {
        List<Item> horizontalItems = new List<Item>();
        horizontalItems.AddRange( CheckDirection( item.Index, Vector2Int.right, item.Type ) );
        horizontalItems.AddRange( CheckDirection( item.Index, Vector2Int.left, item.Type ) );

        List<Item> verticalItems = new List<Item>();
        verticalItems.AddRange( CheckDirection( item.Index, Vector2Int.up, item.Type ) );
        verticalItems.AddRange( CheckDirection( item.Index, Vector2Int.down, item.Type ) );

        List<Item> foundItems = new List<Item>();
        if ( horizontalItems.Count > 1 ) foundItems.AddRange( horizontalItems );
        if ( verticalItems.Count > 1 ) foundItems.AddRange( verticalItems );

        if ( foundItems.Count > 1 )
            foundItems.Insert( 0, item );

        if ( verticalItems.Count > 2 || horizontalItems.Count > 2 )
            item.Mark();

        return foundItems;
    }

    private List<Item> CheckDirection( Vector2Int startIndex, Vector2Int direction, ItemType type )
    {
        List<Item> items = new List<Item>();

        Vector2Int nextIndex = startIndex + direction;

        Item nextItem;

        while ( ( nextItem = GetItemWithIndex( nextIndex ) ) )
        {
            if ( nextItem.Type == type && !nextItem.Destroyed ) items.Add( nextItem );
            else break;

            nextIndex += direction;
        }

        return items;
    }

    private Item GetItemWithIndex( Vector2Int index )
    {
        if ( index.x > _items.GetLength( 0 ) - 1 || index.x < 0 || index.y > _items.GetLength( 1 ) - 1 || index.y < 0 ) return null;

        return _items[ index.x, index.y ];
    }

    private void SwapItems( Item item1, Item item2 )
    {
        Vector2Int item1Index = item1.Index;
        Vector2Int item2Index = item2.Index;

        Vector2 item1Pos = item1.Position;
        Vector2 item2Pos = item2.Position;

        _items[ item2Index.x, item2Index.y ] = item1;
        _items[ item1Index.x, item1Index.y ] = item2;

        item1.MoveTowards( item2Pos );
        item1.SetIndex( item2Index );

        item2.MoveTowards( item1Pos );
        item2.SetIndex( item1Index );
    }

    private void ReplaceItemToSpecial( Item item )
    {
        OnFellItem( new FellItemEvent( item ) );
        ItemType type = item.Type;
        Vector2Int index = item.Index;

        foreach ( Item i in Game.SpecialItemsOrigin )
            if ( i.Type == type )
            {
                _items[ index.x, index.y ] = i;
            }

        _items[ index.x, index.y ] = _itemsSpawner.Create( _items[ index.x, index.y ], index, 0 );
    }

    private void OnFellItem( FellItemEvent data )
    {
        _fallingItems.Remove( data.Item );

        Item.SetBlocking( _fallingItems.Count != 0 );
    }

    private void DestroyItems( List<Item> items, float delay = 0.5f )
    {
        StartCoroutine( _DestroyItems( items, delay ) );
    }

    private IEnumerator _DestroyItems( List<Item> items, float delay = 0.5f )
    {
        while ( _destroyingProcess )
        {
            yield return null;
        }

        _destroyingProcess = true;

        while ( _fallingItems.Count != 0 )
            yield return null;

        yield return new WaitForSeconds( delay );

        foreach ( Item item in items )
        {
            if ( item.Destroyed ) continue;
            item.Destroy();
            if ( item.Marked )
                ReplaceItemToSpecial( item );

        }

        MoveItems();
        _destroyingProcess = false;
    }
}
