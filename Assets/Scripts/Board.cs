﻿using UnityEngine;
using UnityEngine.UI;



public class Board : MonoBehaviour
{
    private RectTransform _rTransform;
    public RectTransform RTransform
    {
        get
        {
            if ( !_rTransform )
                _rTransform = ( RectTransform )transform;

            return _rTransform;
        }
    }



    public Vector2 GetPosByIndex( Vector2Int index )
    {
        float xSpacing = index.x == 0 ? 0 : Game.Params.CellSpacing;
        float ySpacing = index.y == 0 ? 0 : Game.Params.CellSpacing;

        return new Vector2( index.x * ( Game.Params.CellSize + xSpacing ) + Game.Params.BoradPadding, index.y * ( Game.Params.CellSize + ySpacing ) + Game.Params.BoradPadding );
    }


    private void Awake()
    {
        Vector2 totalCellsSize = new Vector2( Game.Params.BoardSize.x, Game.Params.BoardSize.y ) * Game.Params.CellSize;
        Vector2 totalSpacingSize = new Vector2( Game.Params.BoardSize.x - 1, Game.Params.BoardSize.y - 1 ) * Game.Params.CellSpacing;
        Vector2 totalPaddingSize = new Vector2( Game.Params.BoradPadding, Game.Params.BoradPadding ) * 2;

        RTransform.sizeDelta = totalCellsSize + totalSpacingSize + totalPaddingSize;
    }
}
