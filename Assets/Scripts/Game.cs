﻿using System.Collections.Generic;
using UnityEngine;



public class Game : MonoBehaviour
{

    [SerializeField] private GameParams _params;

    [SerializeField] private Item[] _itemsOrigin;
    [SerializeField] private Item[] _specialItemsOrigin;

    [SerializeField] private Board _gameFiled;


    public static GameParams Params;

    public static Item[] ItemsOrigin;
    public static Item[] SpecialItemsOrigin;

    public static Dictionary<ItemType, Item> ItemOriginByType = new Dictionary<ItemType, Item>();

    public static Board GameField;



    private void Awake()
    {
        EventsSystem.InitEvents();

        Params = _params;
        ItemsOrigin = _itemsOrigin;
        SpecialItemsOrigin = _specialItemsOrigin;
        GameField = _gameFiled;

        for ( int i = 0; i < ItemsOrigin.Length; i++ )
            ItemOriginByType.Add( ItemsOrigin[ i ].Type, ItemsOrigin[ i ] );
    }

    private void OnDestroy()
    {
        EventsSystem.InitEvents();
    }
}
