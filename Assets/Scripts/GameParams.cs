﻿using UnityEngine;



[CreateAssetMenu]
public class GameParams : ScriptableObject
{
    public float ItemsSpeed = 500.0f;

    [Space( 10 )]
    public Vector2Int BoardSize = Vector2Int.one * 10;
    public float BoradPadding = 10;
    public float CellSpacing = 10;
    public float CellSize = 40;
}
